import pandas as pd
import logging as log

def stage(data, thresholds = {'abeta42':1100, 'ptau':19.2, 'ttau':242}):

    markers = ('abeta42', 'ptau', 'ttau')
    log.info('Defined thresholds: %s'%thresholds)

    if len(set(thresholds.keys()).intersection(markers)) != 3:
        raise Exception('Invalid thresholds. Should include: %s'%markers)
    others = list(set(data.columns).difference(markers))

    table = []
    for s, e in data.iterrows():
        is_apos =  e['abeta42'] < thresholds['abeta42']
        is_tpos =  e['ptau'] > thresholds['ptau']
        is_npos =  e['ttau'] > thresholds['ttau']
        row = [s, is_apos, is_tpos, is_npos]
        table.append(row)
    columns=['ID', 'A', 'T', 'N']
    df = pd.DataFrame(table, columns=columns).set_index('ID')
    df = df.join(data[others])
    return df

def groups(atn):
    d = {'A+': atn[atn['A']],
         'A-': atn[~atn['A']],
         'T+': atn[atn['T']],
         'T-': atn[~atn['T']],
         'N+': atn[atn['N']],
         'N-': atn[~atn['N']],

         'A+T-': atn[(atn['A'] & ~atn['T'])],
         'A+T+': atn[(atn['A'] & atn['T'])],
         'A-T-': atn[(~atn['A'] & ~atn['T'])],
         'A-T+': atn[(~atn['A'] & atn['T'])],

         'A+T+N+': atn[(atn['A'] & atn['T'] & atn['N'])],
         'A+T+N-': atn[(atn['A'] & atn['T'] & ~atn['N'])],

         'A-T+N+': atn[(~atn['A'] & atn['T'] & atn['N'])],
         'A-T+N-': atn[(~atn['A'] & atn['T'] & ~atn['N'])],

         'A-T-N-': atn[(~atn['A'] & ~atn['T'] & ~atn['N'])],
         'A-T-N+': atn[(~atn['A'] & ~atn['T'] & atn['N'])]
    }
    return d

def staging_summary(atn):
    ans = 'CSF amyloid (A) positive/negative: %s/%s\n'\
		%(len(atn[atn['A']]), len(atn[~atn['A']]))\
        + 'CSF ptau (T) positive/negative: %s/%s\n'\
		%(len(atn[atn['T']]), len(atn[~atn['T']]))\
        + 'CSF ttau (N) positive/negative: %s/%s\n\n'\
		%(len(atn[atn['N']]), len(atn[~atn['N']]))\
        \
        + 'A+T+: %s\n'\
		%len(atn[(atn['A'] & atn['T'])] )\
        + 'A+T-: %s\n'\
        %len(atn[(atn['A'] & ~atn['T'])] )\
        + 'A-T-: %s\n'\
		%len(atn[(~atn['A'] & ~atn['T'])] )\
        + 'A-T+ (SNAPs): %s\n\n'\
		%len(atn[(~atn['A'] & atn['T'])] )\
        \
        + 'A+T+N+: %s\n'\
        %len(atn[(atn['A'] & atn['T'] & atn['N'])] )\
        + 'A+T+N-: %s\n'\
        %len(atn[(atn['A'] & atn['T'] & ~atn['N'])] )\
        \
        + 'A-T+N+: %s\n'\
        %len(atn[(~atn['A'] & atn['T'] & atn['N'])] )\
        + 'A-T+N-: %s\n'\
        %len(atn[(~atn['A'] & atn['T'] & ~atn['N'])] )\
        \
        + 'A-T-N-: %s\n'\
        %len(atn[(~atn['A'] & ~atn['T'] & ~atn['N'])] )\
        + 'A-T-N+: %s\n'\
        %len(atn[(~atn['A'] & ~atn['T'] & atn['N'])] )\
        + 'Total subjects: %s\n'%len(atn)

    return ans
